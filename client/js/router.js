//HOME
Router.route('/', function () {
    this.render('view_home', {
        data: function () {
            var data = {
                eventList:Events.find({})
            };
              return data;
        }
    });
});

//SINGLE EVENT VIEW
Router.route('/event/:_id', function () {
    var params = this.params;
    var id = params._id;
    this.render('view_event', {
        data: function () {
            var data = {
                ev: [Events.findOne({_id: id})],
                guestList: Guests.find({subscribedTo:id})
            };
              return data;
        }
    });
});

//EDIT EVENT VIEW
Router.route('/edit_event/:_id', function () {
    var id = this.params._id;
    this.render('view_edit_event', {
        data: function () {
              return Events.findOne({_id: id});
        }
    });
});

//ADD EVENT VIEW
Router.route('/new_event', function () {
    this.render('view_edit_event');
});

//ADD GUEST VIEW
Router.route('/add_guest/:_id', function () {
    this.render('view_edit_guest',{
        data: function(){
        	var img = Math.floor( Math.random() * 9 );
            return {
            	subscribedTo:this.params._id,
            	img:img
            };
        }
    });
});

//EDIT GUEST VIEW
Router.route('/edit_guest/:gid', function () {
    this.render('view_edit_guest',{
        data: function(){
            return Guests.findOne({_id:this.params.gid});
        }
    });
});