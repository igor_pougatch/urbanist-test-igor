if(Meteor.isClient){

    function returnToEventView(id){
        Router.go("/event/"+id);
    }

    function getData(){
        return {
            guestName:$("#guestName + input").val(),
            attending:$("#attending").is(".active"),
            maybe:$("#maybe").is(".active"),
            willNot:$("#willNot").is(".active"),
            subscribedTo:$(".evId").html(),
            img:$(".userImg").html()
        }
    }

    function validData(data){
        if(data.guestName){
            if(data.maybe || data.attending || data.willNot){
                return true;
            }
        } 
        return false;
    }

    Template.view_edit_guest.events({
        'click .attending-status' : function(e){
            $(".active").removeClass("active");
            $(e.target).addClass("active");
        },
        'click .btn-add-guest': function(){
            var data = getData();
            if(!validData(data)){
                alert("Fill in the required fields");
                return;
            }
            Guests.insert(data);
            returnToEventView(data.subscribedTo);
        },
        'click .btn-update-guest': function(){
            var data = getData();
            if(!validData(data)){
                alert("Fill in the required fields");
                return;
            }
            Guests.update(
                this._id,
                {
                    $set:data
                }
            );
            returnToEventView(data.subscribedTo);
        },
        'click .btn-remove-guest': function(){
            var data = getData();
            Guests.remove({_id:this._id});
            returnToEventView(data.subscribedTo);
        }
    });
}