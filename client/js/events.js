if(Meteor.isClient){
    function getDataFields(){
        var name = $("#name + input").val(),
            addr = $("#addr + input").val(),
            desc = $("#desc + input").val(),
            time = $("#time + input").val();
            date = $("#date + input").val();
        return {
            name:name,
            addr:addr,
            desc:desc,
            time:time,
            date:date
        };
    }

    Template.view_edit_event.events({
        'click .btn-add-event': function(){
            var data = getDataFields();
            if(!data.name){
                alert("fill in name");
                return;
            }
            Events.insert(data,function(err,id){
                if(err){
                    alert(err);
                    return;
                }
                Router.go("/event/"+id);
            });
        },
        'click .btn-update-event': function(){
            var id = this._id;
            var data = getDataFields();
            if(!data.name){
                alert("fill in name");
                return;
            }
            Events.update(
                id,
                {$set: data},
                function(){
                    Router.go("/event/"+id);
                }
            );
        },
        'click .btn-remove-event': function(){
            var id = this._id;
            Events.remove({_id:id});
            var guestsToRemove = Guests.find({subscribedTo:id})
                .map(function (block) {
                        return block._id;
                });
            for(var i = 0; i<guestsToRemove.length; i++){
                Guests.remove({_id:guestsToRemove[i]});
            }
            Router.go("/");
        }
    });

    Template.view_event.events({
        'click .btn-edit-guest': function(){
            Router.go("/edit_guest/"+this._id);
        }
    });
}